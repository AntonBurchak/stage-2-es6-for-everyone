import { callApi } from '../helpers/apiHelper';
// import { createFighterPreview } from '../components/fighterPreview';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      console.log(apiResult);
      console.trace()
      return apiResult;
    } catch (error) {
      throw error;
    } 
  }
}

export const fighterService = new FighterService();
